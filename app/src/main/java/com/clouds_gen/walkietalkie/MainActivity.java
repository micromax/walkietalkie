package com.clouds_gen.walkietalkie;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketFactory;

import java.io.IOException;
import java.util.ArrayList;

import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity {

   // private WebSocketClient client;
   // Requesting permission to RECORD_AUDIO

    public ArrayList<Message> Playlist = new ArrayList<Message>();
    public static WebSocketClient webSocketClient ;
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static String fileName = null;


    private MediaRecorder recorder = null;

    private MediaPlayer player = null;
    public WebSocket ws = null;
    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {
            Manifest.permission.RECORD_AUDIO
            };


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;

        }
        if (!permissionToRecordAccepted ) finish();

    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);


       //client = new WebSocketClient(this);
       // client.run();

       webSocketClient = new WebSocketClient(this);
        webSocketClient.run();
        Thread listenThread = new Thread(new Runnable() {
           @Override
            public void run() {


               AudioCall au = new AudioCall();
                        au.startCall();


            /*   WebSocketFactory factory = new WebSocketFactory().setConnectionTimeout(5000);

               // Create a WebSocket. The timeout value set above is used.
               try {
                   ws = factory.createSocket("ws://34.70.10.163:8080/chat/te2");

                   ws.addListener(new WebSocketAdapter() {
                       @Override
                       public void onTextMessage(WebSocket websocket, String message) throws Exception {
                           Log.d("TAG", "onTextMessage: " + message);
                       }
                   });

                   ws.connectAsynchronously();*/
             //  } catch (IOException e) {
             //      e.printStackTrace();
              // }


                }
        });
        listenThread.start();



    }

    @Override
    public void onPause() {
        super.onPause();

    }






    private void startRecording() {

    }

    private void stopRecording() {

    }

    private void setRecordIcon(boolean record) {

    }

    public void send() {
       // client.sendAudio();
    }
}
