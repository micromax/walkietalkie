package com.clouds_gen.walkietalkie;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.net.InetAddress;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public final class WebSocketClient extends WebSocketListener {
    private static final String LOG_TAG = "WebSocketClient";
    public static final String START = "start";
    public static final String END = "end";

    private final Context mContext;
    public volatile static List<byte[]> sList = new CopyOnWriteArrayList<byte[]>();
    WebSocket mSocket;
    private MediaPlayer mPlayer;


    public WebSocketClient(Context c) {
        mContext = c;
    }

    public void run() {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .build();

        Request request = new Request.Builder()
                .url("ws://34.70.10.163:8080/chat/te2")
                .build();
        client.newWebSocket(request, this);

        // Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
        client.dispatcher().executorService().shutdown();
    }

    @Override
    public void onOpen(final WebSocket webSocket, Response response) {
        Log.d(LOG_TAG, "onOpen: ");
        mSocket = webSocket;
    }

    public void sendAudio(Message m) {
       /* FileChannel in = null;

        try {
         ///   File f = new File(sRecordedFileName);
         //   in = new FileInputStream(f).getChannel();

           /// mSocket.send(START);
*/
        try {
            sendAudioBytes(m);
        } catch (IOException e) {
            e.printStackTrace();
        }
/*
            mSocket.send(END);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        */
    }

    private void sendAudioBytes(Message in) throws IOException {

      //  ByteBuffer buff = ByteBuffer.wrap((byte[]) in.getMessage().toString().getBytes());
        //ByteBuffer buff = ByteBuffer.allocateDirect(32);

        if (in.getMessage().toString().getBytes().length > 0) {

            ObjectMapper objectMapper = new ObjectMapper();

            String s = objectMapper.writeValueAsString(in);
            Log.d(LOG_TAG, "oSend: " + s.toString());
            mSocket.send(s);

        }
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
//        if (text.equals(START)) {
//            sList.clear();
//        } else if (text.equals(END)) {
//            playReceivedFile();
//        } else {
//            try {
//                String hexValue = text.substring(text.indexOf("hex=") + 4, text.length() - 1);
//                ByteString d = ByteString.decodeHex(hexValue);
//                byte[] bytes = d.toByteArray();
//
//                sList.add(bytes);
//            } catch (IllegalArgumentException e) {
//                e.printStackTrace();
//            }
//        }


        try {
            ObjectMapper m = new ObjectMapper();
            String json =  text.toString();

            System.out.println(json);
            m.getTypeFactory();
            m.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
            //lastPacketTime = System.nanoTime();

            Message in = m.readValue(json, Message.class);
            Log.d(LOG_TAG, "onMessage: " + json);
            sList.add(in.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        Log.d(LOG_TAG, "onMessage: " + bytes.toByteArray());
        sList.add(bytes.toByteArray());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        Log.d(LOG_TAG, "onClosing: " + reason);
        webSocket.close(1000, null);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        Log.e(LOG_TAG, "onFailure: ", t);
        t.printStackTrace();
    }

    private void playReceivedFile() {
        File f = buildAudioFileFromReceivedBytes();

        playAudio(f);
    }

    @NonNull
    private File buildAudioFileFromReceivedBytes() {
        File f = new File(mContext.getCacheDir().getAbsolutePath() + "/received.3gp");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        try {
            out = (new FileOutputStream(f));
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            for (byte[] b : sList) {
                out.write(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    private void playAudio(File f) {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mContext, Uri.parse(f.getPath()));
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(LOG_TAG, "onClosing: dudation in millis: " + mPlayer.getDuration());

        mPlayer.start();
    }

}

